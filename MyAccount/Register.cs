﻿using Gallio.Framework;
using Gallio.Model;
using MbUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using PosesFunctionalTests;

namespace PosesFunctionalTests.MyAccount
{
    /// <summary>tests for the sauce labs guinea pig page</summary>
    [TestFixture]
    [Header("browser", "version", "platform")] // name of the parameters in the rows
    [Row("firefox", "25", "Windows 7")] // run all tests in the fixture against firefox 25 for windows 7
    [Row("chrome", "31", "Windows 7")] // run all tests in the fixture against chrome 31 for windows 7
    public class Register
    {

        /// <summary>selenium webdriver interface</summary>
        private IWebDriver _Driver;

        /// <summary>starts a sauce labs sessions</summary>
        /// <param name="browser">name of the browser to request</param>
        /// <param name="version">version of the browser to request</param>
        /// <param name="platform">operating system to request</param>
        private void _Setup(string browser, string version, string platform)
        {
            // construct the url to sauce labs
            Uri commandExecutorUri = new Uri("http://ondemand.saucelabs.com/wd/hub");

            // set up the desired capabilities
            DesiredCapabilities desiredCapabilites = new DesiredCapabilities(browser, version, Platform.CurrentPlatform); // set the desired browser
            desiredCapabilites.SetCapability("platform", platform); // operating system to use
            desiredCapabilites.SetCapability("username", App.Configuration.SauceLabsAccountName); // supply sauce labs username
            desiredCapabilites.SetCapability("accessKey", App.Configuration.SauceLabsAccountKey);  // supply sauce labs account key
            desiredCapabilites.SetCapability("name", TestContext.CurrentContext.Test.Name); // give the test a name

            // start a new remote web driver session on sauce labs
            _Driver = new RemoteWebDriver(commandExecutorUri, desiredCapabilites);
            _Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            _Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(30));

            // navigate to the page under test
            _Driver.Navigate().GoToUrl(App.Configuration.UnderstoodDomain);
        }

        /// <summary>tests the title of the page</summary>
        [Parallelizable, Test, Ignore]
        public void PageTitle(string browser, string version, string platform)
        {
            // start the remote webdriver session with sauce labs
            _Setup(browser, version, platform);

            // verify the page title is correct
            Assert.Contains(_Driver.Title, "Understood");
        }

        /// <summary>tests that the link works on the page</summary>
        [Parallelizable, Test, Ignore]
        public void LinkWorks(string browser, string version, string platform)
        {
            // start the remote webdriver session with sauce labs
            _Setup(browser, version, platform);

            // find and click the link on the page
            var link = _Driver.FindElement(By.LinkText("Sign In"));
            link.Click();

            // wait for the page to change
            WebDriverWait wait = new WebDriverWait(_Driver, TimeSpan.FromSeconds(30));
            wait.Until((d) => { return d.Url.Contains("sign-in"); });

            // verify the browser was navigated to the correct page
            Assert.Contains(_Driver.Url, "poses.dev01.rax.webstagesite.com/en/my-account/sign-in");
        }

        /// <summary>called at the end of each test to tear it down</summary>
        [TearDown]
        public void CleanUp()
        {
            bool passed = TestContext.CurrentContext.Outcome.Status == TestStatus.Passed;
            try
            {
                ((IJavaScriptExecutor)_Driver).ExecuteScript("sauce:job-result=" + (passed ? "passed" : "failed"));
            }
            finally
            {
                _Driver.Quit();
            }
        }

    }
}