﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Westwind.Utilities.Configuration;

namespace PosesFunctionalTests
{
    public class ApplicationConfiguration : AppConfiguration
    {
        public ApplicationConfiguration()
        {
        }

        public string SauceLabsAccountName { get; set; }
        public string SauceLabsAccountKey {get; set; }
        public string UnderstoodDomain { get; set; }
    }
}