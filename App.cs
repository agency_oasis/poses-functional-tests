﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PosesFunctionalTests
{
    public class App
    {

        // static property on any class
        public static ApplicationConfiguration Configuration { get; set; }

        // static constructor ensures this code runs only once 
        // the first time any static property is accessed
        static App()
        {
            /// Load the properties from the Config store
            Configuration = new ApplicationConfiguration();
            Configuration.Initialize();
        }

    }
}